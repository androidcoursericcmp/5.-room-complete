package pe.com.room.example;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import pe.com.room.example.adapter.PalabraAdapter;
import pe.com.room.example.model.Palabra;
import pe.com.room.example.util.Constant;
import pe.com.room.example.util.FunctionsUtil;
import pe.com.room.example.viewmodel.PalabraViewModel;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button butInsertar, butFiltrar, butActualizar, butEliminar, butConsultar;

    private RecyclerView rviPalabra;
    private RecyclerView.LayoutManager layoutManager;
    private PalabraAdapter adapter;

    private PalabraViewModel vmPalabra;

    private List<Palabra> lstPalabra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        butInsertar = findViewById(R.id.butInsertar);
        butFiltrar = findViewById(R.id.butFiltrar);
        butActualizar = findViewById(R.id.butActualizar);
        butEliminar = findViewById(R.id.butEliminar);
        butConsultar = findViewById(R.id.butConsultar);

        butInsertar.setOnClickListener(this);
        butFiltrar.setOnClickListener(this);
        butActualizar.setOnClickListener(this);
        butEliminar.setOnClickListener(this);
        butConsultar.setOnClickListener(this);


        rviPalabra = findViewById(R.id.rviPalabra);

        vmPalabra = ViewModelProviders.of(this).get(PalabraViewModel.class);
        setupRecyclerView();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.butConsultar:
                consultarPalabras();
                break;
            case R.id.butInsertar:
                insertarPalabras();
                break;
            case R.id.butActualizar:
                updatePalabras();
                break;
            case R.id.butEliminar:
                deletePalabras();
                break;
            case R.id.butFiltrar:
                filtarLetraA();
                break;
        }
    }


    private void setupRecyclerView() {

        adapter = new PalabraAdapter();
        layoutManager = new LinearLayoutManager(this);

        //Configuramos nuestro recyclerView y le pasamos valores
        rviPalabra.setHasFixedSize(true);
        rviPalabra.setLayoutManager(layoutManager);
        rviPalabra.setAdapter(adapter);
    }


    private void consultarPalabras() {
        vmPalabra.getAllPalabras().observe(MainActivity.this, new Observer<List<Palabra>>() {
            @Override
            public void onChanged(List<Palabra> lstPalabra) {
                MainActivity.this.lstPalabra = lstPalabra;

                adapter.setList(lstPalabra);

            }
        });
    }

    private void filtarLetraA() {
        vmPalabra.filtarLetraA().observe(MainActivity.this, new Observer<List<Palabra>>() {
            @Override
            public void onChanged(List<Palabra> lstPalabra) {

                adapter.setList(lstPalabra);

            }
        });
    }


    private void insertarPalabras() {
        String jsonStr = FunctionsUtil.loadJSONFromAsset(this, Constant.JSON_INITIALIZER);
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Palabra>>() {
        }.getType();
        List<Palabra> lstPalabra = gson.fromJson(jsonStr, listType);

        vmPalabra.insertPalabras(lstPalabra);
    }

    private void deletePalabras() {

        vmPalabra.delete();
    }

    private void updatePalabras() {
        for (Palabra palabra : lstPalabra) {
            palabra.setWorld(palabra.getWorld() + " - Se modifico");
        }

        vmPalabra.updatePalabras(lstPalabra);
    }

}