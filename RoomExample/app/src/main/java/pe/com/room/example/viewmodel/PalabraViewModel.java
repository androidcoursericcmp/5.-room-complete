package pe.com.room.example.viewmodel;

/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import pe.com.room.example.model.Palabra;


/**
 * View Model to keep a reference to the word repository and
 * an up-to-date list of all words.
 */

public class PalabraViewModel extends AndroidViewModel {

    private PalabraRepository mRepository;

    public PalabraViewModel(Application application) {
        super(application);
        mRepository = new PalabraRepository(application);

    }

    public LiveData<List<Palabra>> getAllPalabras() {

        return mRepository.getAllPalabras();
    }

    public LiveData<List<Palabra>> filtarLetraA() {

        return mRepository.filtarLetraA();
    }

    public void insertPalabras(List<Palabra> lstPalabra) {

        mRepository.insertPalabras(lstPalabra);
    }

    public void updatePalabras(List<Palabra> lstPalabras) {
        mRepository.updatePalabras(lstPalabras);
    }

    public void delete() {
        mRepository.delete();
    }


}