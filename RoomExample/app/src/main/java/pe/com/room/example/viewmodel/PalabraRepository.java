package pe.com.room.example.viewmodel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import pe.com.room.example.db.AppDataBase;
import pe.com.room.example.db.dao.PalabraDAO;
import pe.com.room.example.model.Palabra;

public class PalabraRepository {
    AppDataBase db;
    private PalabraDAO mPalabraDAO;
    private ExecutorService executorService;

    PalabraRepository(Application application) {
        db = AppDataBase.getAppDb(application);

        mPalabraDAO = db.palabraDAO();
        executorService = Executors.newSingleThreadExecutor();

    }


    LiveData<List<Palabra>> getAllPalabras() {
        return mPalabraDAO.listAll();
    }

    LiveData<List<Palabra>> filtarLetraA() {
        return mPalabraDAO.filtarLetraA();
    }


    public void insertPalabras(final List<Palabra> lstPalabra) {

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                mPalabraDAO.insertPalabras(lstPalabra);

            }
        });
    }

    public void updatePalabras(final List<Palabra> lstPalabras) {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mPalabraDAO.updatePalabras(lstPalabras);
            }
        });
    }

    public void delete() {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mPalabraDAO.deleteAll();
            }
        });
    }


}
