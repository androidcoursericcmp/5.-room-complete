package pe.com.room.example.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import pe.com.room.example.model.Palabra;
import pe.com.room.example.util.Constant;

@Dao
public interface PalabraDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPalabras(List<Palabra> lstPalabra);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Palabra palabra);

    @Update
    void updatePalabras(List<Palabra> lstPalabra);

    @Query("SELECT * FROM " + Constant.NAME_TABLE_PALABRA + " ORDER BY world")
    LiveData<List<Palabra>> listAll();

    @Query("SELECT * FROM " + Constant.NAME_TABLE_PALABRA + " WHERE world like 'a%'")
    LiveData<List<Palabra>> filtarLetraA();


    @Query("DELETE FROM " + Constant.NAME_TABLE_PALABRA)
    void deleteAll();


}
